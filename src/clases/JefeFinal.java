package clases;

/*
 * Clase  que guarda los atributos de los jefes finales
 *  @author nachobar
 * 
 */

public class JefeFinal extends Personaje{

	/**
	 * Constructor
	 * genera un nuevo jefe final dados todos los atributos y recibiendo los atributos de la superclase
	 * @param ataqueSupremo nombre del ataque bestia del jefe final
	 * @param ataqueExtra cantidad de ataque extra que a�ade el ataque supremo
	 * @param resurrecion dice si el jefe supremo puede resucitar una vez o no
	 * @param nombre sobreescribe el nombre del atributo Persona para darle un nombre a�adiendo "Lord" al Jefe Final
	 */

	private String ataqueSupremo;
	private int ataqueExtra;
	private boolean resurreccion;

	public JefeFinal(String nombre, int vida, int defensa, int ataque, String arma, String ataqueSupremo,
			int ataqueExtra, boolean resurrecion) {
		super(nombre, vida, defensa, ataque, arma);
		this.nombre = "Lord" + nombre;
		this.ataqueSupremo = ataqueSupremo;
		this.ataqueExtra = ataqueExtra;
		this.resurreccion = resurrecion;
	}

	public String getAtaqueSupremo() {
		return ataqueSupremo;
	}
	public void setAtaqueSupremo(String ataqueSupremo) {
		this.ataqueSupremo = ataqueSupremo;
	}
	public int getAtaqueExtra() {
		return ataqueExtra;
	}
	public void setAtaqueExtra(int ataqueExtra) {
		this.ataqueExtra = ataqueExtra;
	}
	public boolean isResurreccion() {
		return resurreccion;
	}
	public void setResurreccion(boolean resurreccion) {
		this.resurreccion = resurreccion;
	}

	/* primero verifica si el jefe final puede resucitar, en el caso de poder sobreescribe el metodo regenerarVida dandole una vida que recibe como parametro cura
	 * devuelve la vida tras la cura. Si no tiene resurrecciones lo dice
	 * @param el string con la vida a curar del personaje
	 */

	@Override
	public String regenerarVida (String cura) {

		if (resurreccion) {
			setVida(Integer.parseInt(cura));
			resurreccion = false;
			System.out.println("El personaje ha resucitado");
		} else {
			System.out.println("No tienes ninguna resurreci�n");
		}

		return "La vida del personaje es " + vida;
	}

	/*  el Jefe Final pierde el 80% de la vida que tiene disponible para ganar una resurrecci�n sobreescribiendo el atributo Vida de la Superclase. 
	 * El metodo verifica si tiene resjurreciones ya que solo puede tener una
	 * si ya tiene resurrecciones lo dice. El metodo devuelve la vida con la que se queda
	 *	 
	 */

	public String ganarResurrecion() {

		if (!resurreccion) {
			vida -= (vida*80)/100;
			resurreccion = true;
			System.out.println("Has ganado una resurreccion");
		} else {
			System.out.println("Ya tienes resurrecciones");
		}
		return "Te queda " + vida+ " de vida";
	}

	/*se verifica la vida del jefe final y si le queda menos del 5% de la vida se lanza automaticamente el ataque supremo contra el objeto luchador recibido como parametro
	 * el metodo suma el ataque de la superclase personaje al ataqueExtra del ataquesupremo y se lo resta a la vida del objeto Luchador
	 * devuelve la vida restante del luchador. verifica si existe un objeto luchador, si no existe lo dice
	 * @param el objeto Luchador */

	public String ataqueSupremo( Luchador objLuchador){

		if ( objLuchador != null ) {
			if (vida < 50) {
				int ataqueLanzado = ataque + ataqueExtra;
				objLuchador.setVida(objLuchador.getVida()-ataqueLanzado);	
				System.out.println("Se va  alanzar un ataque al luchado por valor de " + ataqueLanzado);
			} else {
				System.out.println("El jefe final aun no puede lanzar el ataque supremo");
			}
		} else {
			System.out.println("El luchador no existe");
		}
		return "La vida restante del luchador es " + objLuchador.getVida();
	}

	@Override
	public String toString() {
		return super.toString()+"JefeFinal ["+"ataqueSupremo=" + ataqueSupremo + ", ataqueExtra=" + ataqueExtra + ", resurrecion="
				+ resurreccion + "]";
	}

}
