package clases;

/*
 * Clase  que guarda los atributos de los luchadores normales
 *  @author nachobar
 * 
 */


public class Luchador extends Personaje {


	/**
	 * Constructor
	 * genera un nuevo luchador dados todos los atributos y recibiendo los atributos de la superclase
	 * @param armadura indica si el luchador tiene un aarmadura
	 * @param armaduraNombre indica el nombre de la armadura del luchador
	 * @param armaduraDefensaExtra que es la defensa que a�ade esa armadura 
	 */

	private boolean armadura; 
	private String armaduraNombre;
	private int armaduraDefensaExtra;

	public Luchador(String nombre, int vida, int defensa, int ataque, String arma, boolean armadura,
			String armaduraNombre, int armaduraDefensaExtra) {
		super(nombre, vida, defensa, ataque, arma);
		this.armadura = armadura;
		this.armaduraNombre = armaduraNombre;
		this.armaduraDefensaExtra = armaduraDefensaExtra;
	}

	public boolean isArmadura() {
		return armadura;
	}
	public void setArmadura(boolean armadura) {
		this.armadura = armadura;
	}
	public String getArmaduraNombre() {
		return armaduraNombre;
	}
	public void setArmaduraNombre(String armaduraNombre) {
		this.armaduraNombre = armaduraNombre;
	}
	public int getArmaduraDefensaExtra() {
		return armaduraDefensaExtra;
	}
	public void setArmaduraDefensaExtra(int armaduraDefensaExtra) {
		this.armaduraDefensaExtra = armaduraDefensaExtra;
	}

	/* verifica si el personaje tiene armadura y le suma la defensa de la armadura sobreescribiendo el tributo defensa de la superclase Personaje, luego muestra el valor de la defensa por pantalla
	 *si no tienearmadura lo dice
	 */

	public String ponerseArmadura () {

		if (armadura) {
			defensa = defensa + armaduraDefensaExtra;
			System.out.println("Te has puesto la armadura");

		} else {
			System.out.println("El personaje no tiene armadura");
		}
		return "La defensa del luchador es " + defensa;
	}

	/*  el Luchador pierde el 50% de la vida que tiene disponible para ganar una armadura sobreescribiendo el atributo Vida de la Superclase. 
	 * El metodo verifica si ya tiene armadura ya que solo puede tener una
	 * si ya tiene armadura lo dice. El metodo devuelve la vida con la que se queda
	 * @param recibe un String con el valor numerico de la armadura
	 *	 
	 */

	public String ganarArmadura(String defensica) {

		if (!armadura) {
			vida -= (vida*50)/100;
			armadura = true;
			armaduraDefensaExtra = Integer.parseInt(defensica);
			System.out.println("Has conseguido una armadura");
		} else {
			System.out.println("Ya tienes tienes una armadura");
		}
		return "Te queda " + vida + " de vida";
	}

	@Override
	public String toString() {
		return super.toString()+"Luchador ["+ "armadura=" + armadura + ", armaduraNombre=" + armaduraNombre + ", armaduraDefensaExtra="
				+ armaduraDefensaExtra + "]";
	}

}
