package clases;

/*
 * SuperClase que guarda los atributos de los personajes
 *  @author nachobar
 * 
 */

public class Personaje {


	/**
	 * Constructor
	 * Uno en blanco genero un nuevo personaje sin recibir parametros
	 * genera un nuevo personaje dados todos los atributos
	 * @param nombre es el identificador que tendra el personaje
	 * @param vida que es la vida total del personaje
	 * @param defensa que es la defensa total del personaje
	 * @param ataque que es el ataque total del persona
	 * @param que es el nombre dle arma del persona
	 */

	protected String nombre;
	protected int vida;
	protected int defensa;
	protected int ataque;
	private String arma;

	public Personaje () {

	}

	public Personaje(String nombre, int vida, int defensa, int ataque, String arma) {

		this.nombre = nombre;
		this.vida = vida;
		this.defensa = defensa;
		this.ataque = ataque;
		this.arma = arma;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getVida() {
		return vida;
	}
	public void setVida(int vida) {
		this.vida = vida;
	}
	public int getDefensa() {
		return defensa;
	}
	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}
	public int getAtaque() {
		return ataque;
	}
	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}
	public String getArma() {
		return arma;
	}
	public void setArma(String arma) {
		this.arma = arma;
	}

	/* cura a un personaje recibiendo un parametro cura numer, verifica si el persona esta vivo yo si ese personaje esta vivo lo cura sumando a vida la cura
	 * 	 * devuelve la vida tras la cura
	 * @param el String con la vida a curar del personaje
	 */

	public String  regenerarVida (String cura) {

		if (vida!= 0) {
			vida += Integer.parseInt(cura); 
			System.out.println("Has regenerado " + " puntos de vida");
		} else {
			System.out.println("El personaje no est� vivo");
		}
		return "La vida del personaje es " + vida;
	}
	
	/*cambia el nombre de un personaje ya creado, devuelve todos los atributos del persona
	 * @param el string del nombre nuevo
	 * 
	 */
	
	public String cambiarNombre(String nombre) {
		
		this.nombre = nombre;
		
		return toString();
	}

	@Override
	public String toString() {
		return "Personaje [nombre=" + nombre + ", vida=" + vida + ", defensa=" + defensa + ", ataque=" + ataque
				+ ", arma=" + arma + "]";
	}

}
