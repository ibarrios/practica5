package programa;

import java.util.Scanner;

import clases.JefeFinal;
import clases.Luchador;
import clases.Personaje;

public class Programa {

	public static void main(String[] args) {


		Scanner input = new Scanner(System.in);
		String opcion;
		String opcionPersonaje;
		String opcionLuchador;
		String opcionJefe;




		do {

			System.out.println("Introduce una opcion o salir para.... salir");
			System.out.println("1.- Crea un Personaje");
			System.out.println("2.- Crea un Luchador");
			System.out.println("3.- Crea un Jefe Final");
			opcion = input.nextLine();



			switch (opcion) {
			case "1":
				System.out.println("Introduce el nombre del personaje");
				String nombre = input.nextLine();
				System.out.println("Introduce la vida del personaje");
				String vida = input.nextLine();
				System.out.println("Introduce la defensa del personaje");
				String defensa = input.nextLine();
				System.out.println("Introduce el ataque del personaje");
				String ataque = input.nextLine();
				System.out.println("Introduce el nombre del arma");
				String arma = input.nextLine();
				Personaje objPersonaje = new Personaje(nombre, Integer.parseInt(vida), Integer.parseInt(defensa), Integer.parseInt(ataque), arma);
		
				do {
					System.out.println("Has creado un Personaje, elije un personaje o escribe salir para.... salir");
					System.out.println("1.- Muestra el personaje con sus atributos");
					System.out.println("2.- Usa el m�todo regenerarVida");
					System.out.println("3.- Usa el metodo cambiarNombre");
					opcionPersonaje = input.nextLine();
					switch (opcionPersonaje) {
					case "1":
						System.out.println("Este es el personaje con todos sus atributos");
						System.out.println(objPersonaje);
						break;
					case "2":
						System.out.println("Vas a usar el metodo regenerarVida, se verificar� si el personaje esta vivo y se le curar�");
						System.out.println("Introduce cuanta vida quieres curar al personaje");
						String curacion = input.nextLine();
						System.out.println(objPersonaje.regenerarVida(curacion));
						break;
					case "3":
						System.out.println("Vas a usar el metodo cambiarNombre");
						System.out.println("Cambia el nombre del personaje ya creado por uno leido por teclado");
						System.out.println("Introduce el nuevo nombre");
						String newName = input.nextLine();
						System.out.println(objPersonaje.cambiarNombre(newName));
						break;
					default:
						if (!opcionPersonaje.equals("salir")) {
							System.out.println("Opcion incorrecta");}
						break;
					}
				} while (opcionPersonaje.equals("salir"));
				break;
			case "2":
				System.out.println("Introduce el nombre del luchador");
				String nombrePJ = input.nextLine();
				System.out.println("Introduce la vida del luchador");
				String vidaPJ = input.nextLine();
				System.out.println("Introduce la defensa del luchador");
				String defensaPJ = input.nextLine();
				System.out.println("Introduce el ataque del luchador");
				String ataquePJ = input.nextLine();
				System.out.println("Introduce el nombre del arma");
				String armaPJ= input.nextLine();
				System.out.println("�Tiene armadura? Responde si o no");
				String eleccionArmadura = input.nextLine();
				Boolean armadura=false;
				if (eleccionArmadura.equals("si")) {
					armadura = true;					
				}
				System.out.println("Introduce el nombre de la armadura");
				String armaduraNombre = input.nextLine();
				System.out.println("Introduce la defensa de la armadura");
				String armaduraDefensaExtra = input.nextLine();
				Luchador objLuchador = new Luchador(nombrePJ, Integer.parseInt(vidaPJ), Integer.parseInt(defensaPJ), Integer.parseInt(ataquePJ), armaPJ, armadura, armaduraNombre, Integer.parseInt(armaduraDefensaExtra));
				do {
					System.out.println("Has creado un luchador, elije un personaje o escribe salir para.... salir");
					System.out.println("1.- Muestra el luchador con sus atributos");
					System.out.println("2.- Usa el metodo ponerseArmadura");
					System.out.println("3.- Usa el m�tido ganarArmadura");
					opcionLuchador = input.nextLine();
					switch (opcionLuchador) {
					case "1":
						System.out.println("Este es el luchador con todos sus atributos");
						System.out.println(objLuchador);
						break;
					case "2":
						System.out.println("Se va a usar el metodo ponerseArmadura");
						System.out.println("El metodo verifica si el luchador tiene armadura para ponerse y sumar� su defensa a la de su stat de defensa");
						System.out.println(objLuchador.ponerseArmadura());
						break;
					case "3":
						System.out.println("Se va a usar el metodo ganarArmadura");
						System.out.println("El metodo verifica si el luchador no tiene armadura y le quita el 50% de su vida actual para conseguirla. Hay que introducir el valor de la armadura nueva");
						System.out.println("Introduce el valor de la armadura");
						String valorArmadura = input.nextLine();
						System.out.println(objLuchador.ganarArmadura(valorArmadura));
						break;
					default:
						if (!opcionLuchador.equals("salir")) {
							System.out.println("Opcion incorrecta");}
						break;
					}
				} while (opcionLuchador.equals("salir"));
				break;
			case "3":
				System.out.println("Introduce el nombre del jefe final");
				String nombreJF = input.nextLine();
				System.out.println("Introduce la vida del jefe final");
				String vidaJF = input.nextLine();
				System.out.println("Introduce la defensa del jefe final");
				String defensaJF = input.nextLine();
				System.out.println("Introduce el ataque del jefe final");
				String ataqueJF = input.nextLine();
				System.out.println("Introduce el nombre del arma");
				String armaJF = input.nextLine();
				System.out.println("Introduce el nombre del ataque supremo");
				String ataqueSupremo = input.nextLine();
				System.out.println("Introduce el ataque extra");
				String ataqueExtra = input.nextLine();
				System.out.println("�Puede resucitar? Responde si o no");
				String opcionResurreccion = input.nextLine();
				Boolean resurreccion = false;
				if (opcionResurreccion.equals("si")) {
					resurreccion = true;
				}
				JefeFinal objJefeFinal = new JefeFinal(nombreJF, Integer.parseInt(vidaJF), Integer.parseInt(defensaJF), Integer.parseInt(ataqueJF),armaJF, ataqueSupremo, Integer.parseInt(ataqueExtra), resurreccion);
				do {
					System.out.println("Has creado un jefe final, elije un personaje o escribe salir para.... salir");
					System.out.println("1.- Muestra el jefe final con sus atributos");
					System.out.println("2.- Usa el metodo sobreescrito regenerarVida");
					System.out.println("3.- Usa el metodo ganarResurreccion");
					System.out.println("4.- Usa el metodo ataqueSupremo");
					opcionJefe = input.nextLine();
					switch (opcionJefe) {
					case "1":
						System.out.println("Este es el jefe final con todos sus atributos");
						System.out.println(objJefeFinal);
						break;
					case "2":
						System.out.println("Se va a usar el metodo regenerarVida");
						System.out.println("Verifica si el jefe final tiene alguna resurreccion y si su vida es 0 la usa para resucitarlo con la vida introducida");
						System.out.println("Introduce la vida a regenerar");
						String curacionJefe =input.nextLine();
						System.out.println(objJefeFinal.regenerarVida(curacionJefe));
						break;
					case "3":
						System.out.println("Se va a usar el metodo ganarResurrecion");
						System.out.println("Se verifica si el Jefe final tiene alguna resurreccion y si no la tiene usar el 80% de su vida para crear una");
						System.out.println(objJefeFinal.ganarResurrecion());
						break;
					case "4":
						System.out.println("Se va a usar el metodo ataqueSupremo");
						System.out.println("Se verifica si el Jefe Final tiene la vida critica inferior a 50 puntos y se lanza el ataque supremo que quita a la vida de un objeto Luchador ya creado el total de ataque del jefe");
						Luchador objLuchador2 = new Luchador("Pedro", 5000, 300, 500, "Josefa", true, "Lorenza", 500);
						System.out.println(objJefeFinal.ataqueSupremo(objLuchador2));						
						break;
					default:
						if (!opcionJefe.equals("salir")) {
							System.out.println("Opcion incorrecta");}
						break;
					}
				} while (opcionJefe.equals("salir"));
				break;	
			default:
				if (!opcion.equals("salir")) {
					System.out.println("Opcion incorrecta");}
				break;
			}

		} while (!opcion.equals("salir"));
		System.out.println("Has salido del programa");
		input.close();
	}

}
